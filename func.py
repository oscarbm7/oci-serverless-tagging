##########################################################################
# 
# DISCLAIMER – This is not an official Oracle application, It does not supported by Oracle Support
#
# @author: Javier Bernal
#
# Supports Python 3 and above
#
# coding: utf-8
##########################################################################


import io
import os
import json
import sys
import oci
import logging
from fdk import response

def handler(ctx, data: io.BytesIO = None):
    logging.getLogger().info('signer request')  # authentication based on instance principal
    signer = oci.auth.signers.get_resource_principals_signer()
    
    try:        
        body = json.loads(data.getvalue())
        logging.getLogger().info('request body: {0}'.format(str(body)))        
    except Exception as e:
        error = 'Input a JSON object in the format: ' + e
        logging.getLogger().error('ERROR: ' + error)
        raise Exception(error)

    resp = do(signer, body)

    return response.Response(
        ctx, response_data=json.dumps(resp),
        headers={"Content-Type": "application/json"}
    )

def do(signer, body):
    try:
        compartmentId = body["data"]["compartmentId"]
        resourceId = body["data"]["resourceId"]
        eventType = body["eventType"]

        logging.getLogger().info("Request received")
        print(eventType)  
        print(compartmentId)
        print(resourceId)

        core_client = oci.core.ComputeClient(config={}, signer=signer)
        core_client_block = oci.core.BlockstorageClient(config={}, signer=signer)

        if eventType.lower().find("attachvolume") > 0:  

            volume_attachment_id = resourceId

            get_volume_attachment_response = core_client.get_volume_attachment(
                volume_attachment_id=volume_attachment_id)     

            get_instance_response = core_client.get_instance(
                instance_id=get_volume_attachment_response.data.instance_id)

            defined_tags = get_instance_response.data.defined_tags

            update_volume_response = core_client_block.update_volume(
                volume_id=get_volume_attachment_response.data.volume_id,
                update_volume_details=oci.core.models.UpdateVolumeDetails(
                    defined_tags=defined_tags))

        elif eventType.lower().find("detachvolume") > 0:    

            volume_attachment_id = resourceId         
            
            get_volume_attachment_response = core_client.get_volume_attachment(
                volume_attachment_id=volume_attachment_id)     

            get_instance_response = core_client.get_instance(
                instance_id=get_volume_attachment_response.data.instance_id)

            defined_tags = get_instance_response.data.defined_tags

            oracle_tags = defined_tags["Oracle-Tags"]     
            print("oracle_tags")
            print(oracle_tags)

            defined_tags = {"Oracle-Tags": oracle_tags}

            update_volume_response = core_client_block.update_volume(
                volume_id=get_volume_attachment_response.data.volume_id,
                update_volume_details=oci.core.models.UpdateVolumeDetails(
                    defined_tags=defined_tags))

        else:
            #Update instance taggin target: Boot and Block Volume
            #Boot volume tagging

            #Get Instance
            get_instance_response = core_client.get_instance(instance_id=resourceId)
            defined_tags = get_instance_response.data.defined_tags
            availabilityDomain = get_instance_response.data.availability_domain

            #print(availabilityDomain)
            #print(get_instance_response.data.defined_tags)

            try:
                #Get boot volume
                list_boot_volume_response = core_client.list_boot_volume_attachments( 
                    availability_domain=availabilityDomain, compartment_id=compartmentId, 
                    instance_id=resourceId)        

                boot_volume_id = list_boot_volume_response.data[0].boot_volume_id
                            
                update_boot_volume_response = core_client_block.update_boot_volume(
                    boot_volume_id=boot_volume_id, 
                    update_boot_volume_details=oci.core.models.UpdateBootVolumeDetails(
                        defined_tags = defined_tags))


                list_volume_attachments_response = core_client.list_volume_attachments( 
                    compartment_id=compartmentId, instance_id=resourceId)            

                for volume in list_volume_attachments_response.data:
                    
                    update_volume_response = core_client_block.update_volume( 
                        volume_id=volume.volume_id, 
                        update_volume_details=oci.core.models.UpdateVolumeDetails(
                            defined_tags=defined_tags))

            except Exception as e:
                logging.getLogger().info("Duplicate Info")                 
        
        logging.getLogger().info("tagging process complete")

    except Exception as e:
        logging.getLogger().error("Failed:" + str(e))
        raise SystemExit(str(e))
        
    response = {
        "content": "OK"
    }
    return response