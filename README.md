# oci-serverless-tagging

## DISCLAIMER
This is not an official Oracle application, It does not supported by Oracle Support,  please try it and feel free to contribute to improve it.


## Getting started

This project helps us to tag OCI compute storage resources automatically in order to control our costs in cloud subscription.

The main use is to ensure that the boot disks and attached disks use the same tags as the virtual machines (example: cost center, LOB, IT owner, country), this avoids a lot of clicks on the OCI console.

### pre-requirements 
Tags and cost tracking configure

https://docs.oracle.com/en-us/iaas/Content/Tagging/Tasks/usingcosttrackingtags.htm

Create OCI Serverless APP (name: tagging)

https://docs.public.oneportal.content.oci.oraclecloud.com/en-us/iaas/Content/Functions/Tasks/functionsquickstartcloudshell.htm#functionsquickstart_cloudshell

### Installation steps

Clone this repository in our OCI Cloud Shell

Compile OCI serverless funcion

    `fn -v deploy --app tagging`

Create rule trigger

https://docs.oracle.com/en-us/iaas/Content/Events/Task/create-events-rule.htm

The above event rule should have the following event type:

* Instance - Launch End	
* Volume - Attach End	
* Instance - Update	
* Volume - Detach End

And invoke oci function named auto-tagging
